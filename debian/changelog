r-cran-commonmark (1.9.2-2) unstable; urgency=medium

  * Fix clean target
    Closes: #1048735
  * Add missing docs to pass autopkgtest
    Closes: #1088546

 -- Andreas Tille <tille@debian.org>  Tue, 10 Dec 2024 18:30:50 +0100

r-cran-commonmark (1.9.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Fri, 11 Oct 2024 08:42:09 +0900

r-cran-commonmark (1.9.1-1) unstable; urgency=medium

  * New upstream version
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Tue, 13 Feb 2024 13:49:24 +0100

r-cran-commonmark (1.9.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 12 Jun 2023 15:59:29 +0200

r-cran-commonmark (1.8.1-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 24 Oct 2022 13:42:50 +0200

r-cran-commonmark (1.8.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #965980  (CVE-2020-5238)
    Closes: #1006760 (CVE-2022-24724)
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 09 Mar 2022 20:23:20 +0100

r-cran-commonmark (1.7-2) unstable; urgency=medium

  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * rename debian/tests/control.autodep8 to debian/tests/control
    (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Remove empty debian/tests/control.
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Wed, 02 Sep 2020 16:33:27 +0200

r-cran-commonmark (1.7-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 05 Dec 2018 10:51:40 +0100

r-cran-commonmark (1.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Thu, 04 Oct 2018 07:30:57 +0200

r-cran-commonmark (1.5-1) unstable; urgency=medium

  * Initial release (closes: #898223)

 -- Andreas Tille <tille@debian.org>  Tue, 08 May 2018 23:05:00 +0200
